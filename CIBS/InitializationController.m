//
//  InitializationController.m
//  CIBS_PRO
//
//  Created by brst on 20/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "InitializationController.h"

@interface InitializationController ()
- (IBAction)ButtonActions:(UIButton *)sender;

@end

@implementation InitializationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    


}


- (void)viewWillAppear:(BOOL)animated{

    self.navigationItem.title = @"CIBS_PRO";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ButtonActions:(UIButton *)sender{

    
    if (sender.tag==0)
        [self performSegueWithIdentifier:@"EditUserController" sender:self];
    else
        [self performSegueWithIdentifier:@"VideoController" sender:self];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
