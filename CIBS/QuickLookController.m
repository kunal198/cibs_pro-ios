//
//  QuickLookController.m
//  CIBS_PRO
//
//  Created by brst on 20/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "QuickLookController.h"

@interface QuickLookController ()
- (IBAction)segueAtion:(id)sender;

@end

@implementation QuickLookController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;


}
- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationItem.title = @"Quick Setup options";
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)segueAtion:(id)sender {
    
    [self performSegueWithIdentifier:@"RegisterClass" sender:self];
    
}
@end
