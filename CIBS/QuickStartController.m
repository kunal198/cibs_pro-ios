//
//  QuickStartController.m
//  CIBS_PRO
//
//  Created by brst on 20/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "QuickStartController.h"

@interface QuickStartController ()
- (IBAction)QuickSetupAction:(id)sender;

@end

@implementation QuickStartController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.topItem.title = @"Back";

}

- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationItem.title = @"Quick Or Custom";
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)QuickSetupAction:(UIButton*)sender {
    
    if (sender.tag==1)
    {
        [self performSegueWithIdentifier:@"QuickSetupScreen" sender:self];
    }
    
    else
    {
     [self performSegueWithIdentifier:@"Registration" sender:self];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"showRegister"];
    }
    
    
    
}
@end
