//
//  ViewController.m
//  CIBS_PRO
//
//  Created by Brst on 12/7/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "ViewController.h"
#define NUMBERS_ONLY 0123456789
#import "SWRevealViewController.h"
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;

@end

@implementation ViewController

#pragma mark - ViewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
// Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255.0 green:146.0/255.0 blue:63.0/255.0 alpha:1.0];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
  [self.view addGestureRecognizer:tap];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;

   [self settingNumberPadButtons];
        [self setUITextFieldCornerRadius];
    
    self.navigationController.navigationItem.leftBarButtonItems;
    self.navigationItem.title = @"Sign Up";
//    SWRevealViewController *revealViewController = self.revealViewController;
//    if ( revealViewController )
//    {
//        [self.sidebarButton setTarget: self.revealViewController];
//        [self.sidebarButton setAction: @selector( revealToggle: )];
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    }
//    
 
}

- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationItem.title = @"Register";
    
   
    
}

-(void)setUITextFieldCornerRadius
{
    _lastname_txtField.layer.borderWidth = 2.0f;
    _lastname_txtField.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0].CGColor;
    [_lastname_txtField.layer setCornerRadius:8.0f];
    
    
    
    _phone_txtField.layer.borderWidth = 2.0f;
    _phone_txtField.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0].CGColor;
    [_phone_txtField.layer setCornerRadius:8.0f];
    
    
    
    _password_txtField.layer.borderWidth = 2.0f;
    _password_txtField.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0].CGColor;
    [_password_txtField.layer setCornerRadius:8.0f];
    
    
    
    _firstname_txtfield.layer.borderWidth = 2.0f;
    _firstname_txtfield.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0].CGColor;
    [_firstname_txtfield.layer setCornerRadius:8.0f];
    
    _email_txtField.layer.borderWidth = 2.0f;
    _email_txtField.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0].CGColor;
    [_email_txtField.layer setCornerRadius:8.0f];

}




#pragma mark - settingNumberPadButtons
-(void)settingNumberPadButtons
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _phone_txtField.inputAccessoryView = numberToolbar;
    _password_txtField.inputAccessoryView = numberToolbar;
    
}


#pragma mark - cancelNumberPad
-(void)cancelNumberPad
{
    //    [numberTextField resignFirstResponder];
    //    numberTextField.text = @"";
    [self.view endEditing:YES];
}


#pragma mark - doneWithNumberPad
-(void)doneWithNumberPad
{
    //    NSString *numberFromTheKeyboard = numberTextField.text;
    //    [numberTextField resignFirstResponder];
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}



#pragma mark - DismissKeyboard
-(void)dismissKeyboard
{
    [[self view]endEditing:YES];
}


#pragma mark - Save btn Action
- (IBAction)saveBtn:(id)sender
{
    
//    [self performSegueWithIdentifier:@"Searchlocation" sender:self];
//    return;
//
    // Email Validations
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    // Password Validations
    BOOL valid;
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:_password_txtField.text];
    valid = [alphaNums isSupersetOfSet:inStringSet];
    
    
    
    //Blank fields validation
    if ([_firstname_txtfield.text  isEqualToString: @""] && [_lastname_txtField.text  isEqualToString: @""] && [_password_txtField.text  isEqualToString: @""] && [_phone_txtField.text  isEqualToString: @""])
    {
        UIAlertView *alert_view = [[UIAlertView alloc]
                                   initWithTitle:@""
                                   message:@"Please enter all the mandatory fields"
                                   delegate:self
                                   cancelButtonTitle:@"OK"
                                   
                                   otherButtonTitles:nil];
        [alert_view show];
        
        return;
        
    }
    else if ([_firstname_txtfield.text  isEqualToString: @""])
    {
        UIAlertView *alert_view = [[UIAlertView alloc]
                                   initWithTitle:@""
                                   message:@"Please enter first name."
                                   delegate:self
                                   cancelButtonTitle:@"OK"
                                   
                                   otherButtonTitles:nil];
        [alert_view show];
        
        return;
    }
    else if ([_lastname_txtField.text  isEqualToString: @""])
    {
        UIAlertView *alert_view = [[UIAlertView alloc]
                                   initWithTitle:@""
                                   message:@"Please enter last name."
                                   delegate:self
                                   cancelButtonTitle:@"OK"
                                   
                                   otherButtonTitles:nil];
        [alert_view show];
        
        return;

    }
    else if ([_password_txtField.text  isEqualToString: @""])
    {
        UIAlertView *alert_view = [[UIAlertView alloc]
                                   initWithTitle:@""
                                   message:@"Please enter password."
                                   delegate:self
                                   cancelButtonTitle:@"OK"
                                   
                                   otherButtonTitles:nil];
        [alert_view show];
        return;

    }
    else if (([_password_txtField.text length] > 4) || (!valid)) // Not numeric
    {
        UIAlertView *alert_view = [[UIAlertView alloc]
                                   initWithTitle:@""
                                   message:@"Password should be of 4 digits and must contain numerics."
                                   delegate:self
                                   cancelButtonTitle:@"OK"
                                   
                                   otherButtonTitles:nil];
        [alert_view show];
        return;

        
    }
    else if (([_phone_txtField.text  isEqualToString: @""]) ||  ([_phone_txtField.text length] <= 9) || ([_phone_txtField.text length] > 10))
    {
        UIAlertView *alert_view = [[UIAlertView alloc]
                                   initWithTitle:@""
                                   message:@"Please enter 10 digit phone number."
                                   delegate:self
                                   cancelButtonTitle:@"OK"
                                   
                                   otherButtonTitles:nil];
        [alert_view show];
        
        return;

    }
//    else if ([_email_txtField.text  isEqualToString: @""])
//    {
//        UIAlertView *alert_view = [[UIAlertView alloc]
//                                   initWithTitle:@""
//                                   message:@"Please enter valid email."
//                                   delegate:self
//                                   cancelButtonTitle:@"OK"
//                                   
//                                   otherButtonTitles:nil];
//        [alert_view show];
//        
//        return;
//
//    }
    else if (![_email_txtField.text isEqualToString:@""])
    {
        if ([emailTest evaluateWithObject:_email_txtField.text] == NO )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter correct email" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            return;
        }
        else
        {
            NSString *firstname = _firstname_txtfield.text;
            [[NSUserDefaults standardUserDefaults] setValue:firstname forKey:@"firstname"];
            NSString *lastname = _lastname_txtField.text;
            [[NSUserDefaults standardUserDefaults] setValue:lastname forKey:@"lastname"];
            NSString *password = _password_txtField.text;
            [[NSUserDefaults standardUserDefaults] setValue:password forKey:@"password"];
            NSString *phone = _phone_txtField.text;
            [[NSUserDefaults standardUserDefaults] setValue:phone forKey:@"phone"];
            NSString *email = _email_txtField.text;
            [[NSUserDefaults standardUserDefaults] setValue:email forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self performSegueWithIdentifier:@"Searchlocation" sender:self];
        }
//

    }
    else
    {
        NSString *firstname = _firstname_txtfield.text;
        [[NSUserDefaults standardUserDefaults] setValue:firstname forKey:@"firstname"];
        NSString *lastname = _lastname_txtField.text;
        [[NSUserDefaults standardUserDefaults] setValue:lastname forKey:@"lastname"];
        NSString *password = _password_txtField.text;
        [[NSUserDefaults standardUserDefaults] setValue:password forKey:@"password"];
        NSString *phone = _phone_txtField.text;
        [[NSUserDefaults standardUserDefaults] setValue:phone forKey:@"phone"];
        NSString *email = _email_txtField.text;
        [[NSUserDefaults standardUserDefaults] setValue:email forKey:@"email"];
        [[NSUserDefaults standardUserDefaults] synchronize];
            [self performSegueWithIdentifier:@"Searchlocation" sender:self];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Searchlocation"])
    {

        UIViewController *VC=segue.destinationViewController;
        
        
    }
}


#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.returnKeyType==UIReturnKeyNext)
    {
        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    }
    else if (textField.returnKeyType==UIReturnKeyDone)
    {
        [textField resignFirstResponder];
    }
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.tag==1) || (textField.tag==2))
    {
        if ([textField.text length]<=29)
        {
            return YES;
        }
        else if([@"" isEqualToString:string])
        {
            textField.text=[textField.text substringToIndex:29 ];
        }
        
        return NO;
    }
    else if (textField.tag==3)
    {
        if ([textField.text length]<=3)
        {
            return YES;
        }
        else if([@"" isEqualToString:string])
        {
            textField.text=[textField.text substringToIndex:3 ];
        }
        
        return NO;
    }
    else if (textField.tag==4)
    {
        if ([textField.text length]<=9)
        {
//            /*  limit to only numeric characters  */
//            
//            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
//            for (int i = 0; i < [string length]; i++) {
//                unichar c = [string characterAtIndex:i];
//                if ([myCharSet characterIsMember:c]) {
//                    return YES;
//                }
//            }
            
            return YES;

        }
        else if([@"" isEqualToString:string])
        {
            textField.text=[textField.text substringToIndex:9];
        }
        
        return NO;
    }
    else
    {
        return YES;
    }
}


#pragma mark - DidReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sidebarBtn:(id)sender {
    
    
}
@end
