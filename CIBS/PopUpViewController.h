//
//  PopUpViewController.h
//  CIBS
//
//  Created by Brst on 12/28/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PopUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UITextField *paswordTxtf;
@property (weak, nonatomic) IBOutlet UILabel *captionLbl;

- (void)showInView:(UIView *)aView animated:(BOOL)animated;

@end
