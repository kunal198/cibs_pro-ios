//
//  ViewController.h
//  CIBS_PRO
//
//  Created by Brst on 12/7/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *firstName_Lbl;
@property (strong, nonatomic) IBOutlet UILabel *lastName_Lbl;
@property (strong, nonatomic) IBOutlet UILabel *password_Lbl;
@property (strong, nonatomic) IBOutlet UILabel *phoneNumber_Lbl;
@property (strong, nonatomic) IBOutlet UILabel *email_Lbl;


@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@property (strong, nonatomic) IBOutlet UITextField *firstname_txtfield;
@property (strong, nonatomic) IBOutlet UITextField *lastname_txtField;
@property (strong, nonatomic) IBOutlet UITextField *password_txtField;
@property (strong, nonatomic) IBOutlet UITextField *phone_txtField;
@property (strong, nonatomic) IBOutlet UITextField *email_txtField;

@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
- (IBAction)saveBtn:(id)sender;

- (void)textFieldDidEndEditing:(UITextField *)textField;


@end

