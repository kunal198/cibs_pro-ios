//
//  AppDelegate.h
//  CIBS
//
//  Created by Brst on 12/28/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (strong, nonatomic) NSMutableArray *arrayofcontact;

@property (strong, nonatomic) NSMutableString *arratofLocation;


@property (strong, nonatomic) NSMutableArray *LocationsArray;


@property (strong, nonatomic) NSString *pathString;

- (void)saveContext;

@end
