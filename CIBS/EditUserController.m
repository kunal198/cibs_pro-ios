//
//  EditUserController.m
//  CIBS_PRO
//
//  Created by brst on 20/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "EditUserController.h"

@interface EditUserController ()
- (IBAction)VideoToUserEdit:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *nameLbl;
@property (retain, nonatomic) IBOutlet UITextField *nametxtf;
@property (retain, nonatomic) IBOutlet UILabel *numberLbl;
@property (retain, nonatomic) IBOutlet UITextField *numbertxtf;

@end

@implementation EditUserController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    self.navigationController.navigationBar.topItem.title = @"Back";

    _nametxtf.text=[[UIDevice currentDevice] name];
    _numbertxtf.text=@"";
    
    
    _nametxtf.layer.borderWidth = 2.0f;
    _nametxtf.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0].CGColor;
    [_nametxtf.layer setCornerRadius:8.0f];
    
    
    
    _numbertxtf.layer.borderWidth = 2.0f;
    _numbertxtf.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0].CGColor;
    [_numbertxtf.layer setCornerRadius:8.0f];

    

        UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        numberToolbar.barStyle = UIBarStyleBlackTranslucent;
        numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
        [numberToolbar sizeToFit];
        _numbertxtf.inputAccessoryView = numberToolbar;
    
    
}

-(void)cancelNumberPad{
    [_numbertxtf resignFirstResponder];
    _numbertxtf.text = @"";
}

-(void)doneWithNumberPad{
    NSString *numberFromTheKeyboard = _numbertxtf.text;
    [_numbertxtf resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationItem.title = @"Edit User Detail";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)VideoToUserEdit:(id)sender {
    
    
    if (!(_nametxtf.text.length>0)) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"CIBS" message:@"Please enter device name." delegate:self    cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.show;
         return;
    }
    else if (!(_numbertxtf.text.length>0)){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"CIBS" message:@"Please enter Mobile no." delegate:self    cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.show;
        return;
        
    }else{
    
        [self performSegueWithIdentifier:@"QyuckStartController" sender:self];

    }
    

}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.returnKeyType==UIReturnKeyNext)
    {
        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    }
    else if (textField.returnKeyType==UIReturnKeyDone)
    {
        [textField resignFirstResponder];
    }
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag==12)
    {
        if ([textField.text length]<=29)
        {
            return YES;
        }
        else if([@"" isEqualToString:string])
        {
            textField.text=[textField.text substringToIndex:29 ];
        }
        
        return NO;
    }
 
    else if (textField.tag==13)
    {
        if ([textField.text length]<=9)
        {
            
            return YES;
            
        }
        else if([@"" isEqualToString:string])
        {
            textField.text=[textField.text substringToIndex:9];
        }
        
        return NO;
    }
    else
    {
        return YES;
    }
}


@end
