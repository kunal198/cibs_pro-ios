//
//  CameraVewController.h
//  CIBS_PRO
//
//  Created by mrinal khullar on 12/14/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraVewController : UIViewController <UIActionSheetDelegate>
@property (retain, nonatomic) IBOutlet UIImageView *image_view;
- (IBAction)TakeSnapAction:(UIButton *)sender;
- (IBAction)Continue:(UIButton *)sender;

@end
