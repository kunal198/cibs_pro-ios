//
//  ContactListCell.h
//  CIBS_PRO
//
//  Created by mrinal khullar on 12/14/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdatedContactListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UIButton *selectbutton;

@end
