//
//  SettingsViewController.m
//  CIBS_PRO
//
//  Created by brst on 14/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "_SideBarSettingsViewController.h"
#import "SWRevealViewController.h"
#import "PopUpViewController.h"
#import "CIBScallingViewController.h"

@interface _SideBarSettingsViewController (){

    bool isPickerDisplay;
    
        NSMutableArray *pickerNames;
        NSMutableArray *pickerValues;
        NSString *pickerString;
    
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (retain, nonatomic) IBOutlet UIView *pickerBaseView;
@property (retain, nonatomic) IBOutlet UIPickerView *pickerView;
@property (retain, nonatomic) IBOutlet UILabel *distancelbl;
@property (retain, nonatomic) IBOutlet UIButton *homeButton;
@property (retain, nonatomic) IBOutlet UIButton *homebuttonradious;
@property (retain, nonatomic) IBOutlet UIButton *overLapbutton;


- (IBAction)cancelButtonActon:(id)sender;
- (IBAction)doneButtonAction:(id)sender;

@end

@implementation _SideBarSettingsViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"presetSettings"])
//    {
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    } //    }
    

    //Time
    pickerNames = [[NSMutableArray alloc] initWithObjects:@"15 mnts", @"30 mnts", @"1 hrs", @"8 hrs", @"24 hrs", nil];
    //Distance
    pickerValues = [[NSMutableArray alloc] initWithObjects:@"500 mts", @"1 kms", @"2 kms", @"5 kms", @"10 kms", nil];
    
    _pickerView.delegate=self;
    _pickerView.dataSource=self;

    [_overLapbutton setEnabled:NO];

    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"_emergencyCall_btn"])
    {
            [_emergencyCall_btn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_emergencyCall_btn"];
        
    }
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"_emergencyText_btn"])
    {
        [_emergencyText_btn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_emergencyText_btn"];
    }

    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"_emergencyEmail_btn"])
    {
        [_emergencyEmail_btn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_emergencyEmail_btn"];
        

    }
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"_notificationRingtone_btn"])
    {
        [_notificationRingtone_btn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_notificationRingtone_btn"];
        
    }
    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"_notificationViberate_ttn"])
    {
        [_notificationViberate_ttn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_notificationViberate_ttn"];
    }

    
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"shake_btn"])
    {
        [_shakeBtn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        
    }
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"_audio_btn"])
    {
        [_audio_btn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        
    }
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"_video_btn"])
    {
        [_video_btn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        
    }
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"_none_btn"])
    {
        [_none_btn setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        
    }
    
    //ForButtons
    [_audio_btn setBackgroundImage:[UIImage imageNamed:@"Circle Image"] forState:UIControlStateNormal];
    [_video_btn setBackgroundImage:[UIImage imageNamed:@"Circle Image"] forState:UIControlStateNormal];
    [_none_btn setBackgroundImage:[UIImage imageNamed:@"Circle Filled Image"] forState:UIControlStateNormal];

    
    
    
    
    

    
    //LeaveSafeLocation
    [_safelocationRadiusBtn setEnabled:NO];
    _safelocation_lbl.textColor = [UIColor darkGrayColor];
    _locationRadiusValue_lbl.textColor = [UIColor darkGrayColor];
    [_leaveSafeLocationBtn setBackgroundImage:[UIImage imageNamed:@"unchecked white box"] forState:UIControlStateNormal];
    
    
    _distancelbl.textColor = [UIColor darkGrayColor];
    [_homeButton setEnabled:NO];
    [_homebuttonradious setEnabled:NO];

    
    
    [_personalMessage_TxtView.layer setBackgroundColor: [[UIColor clearColor] CGColor]];
    [_personalMessage_TxtView.layer setBorderColor: [[UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0] CGColor]];
    [_personalMessage_TxtView.layer setBorderWidth: 2.0];
    [_personalMessage_TxtView.layer setCornerRadius:8.0f];
    [_personalMessage_TxtView.layer setMasksToBounds:YES];
 
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.scrollView addGestureRecognizer:tap];



    _personalMessage_TxtView.text = @"Personal message";
    _personalMessage_TxtView.textColor = [UIColor darkGrayColor];
    _personalMessage_TxtView.delegate = self;
    
    
//    self.navigationController.navigationBar.topItem.title = @"Back";

    
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    _personalMessage_TxtView.text = @"";
    _personalMessage_TxtView.textColor = [UIColor whiteColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(_personalMessage_TxtView.text.length == 0){
        _personalMessage_TxtView.textColor = [UIColor lightGrayColor];
        _personalMessage_TxtView.text = @"Personal message";
        [_personalMessage_TxtView resignFirstResponder];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

-(void)dismissKeyboard
{
    [_personalMessage_TxtView resignFirstResponder];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
  
    if (pickerView.tag==1001) {
        
       return [pickerNames count];
        
    }
    else{
        return [pickerValues count];
        }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSLog(@">>=%@",pickerNames);
    
    if (pickerView.tag==1001) {
        
        pickerString=[pickerNames objectAtIndex:row];
        
        return [pickerNames objectAtIndex:row];

        
    }else{
        
        pickerString=[pickerValues objectAtIndex:row];
        
        return [pickerValues objectAtIndex:row];
        
    }    return @"";

}



-(void)hideShowPickerView {
    
    if (!isPickerDisplay) {
        
        [UIView animateWithDuration:0.25 animations:^{
            CGRect temp = self.pickerBaseView.frame;
            temp.origin.y = self.view.frame.size.height - self.pickerBaseView.frame.size.height;
            self.pickerBaseView.frame = temp;
        } completion:^(BOOL finished) {
            NSLog(@"picker displayed");
            isPickerDisplay = YES;
        }];
        
    }else {
        
        [UIView animateWithDuration:0.25 animations:^{
            CGRect temp = self.pickerBaseView.frame;
            temp.origin.y = self.view.frame.size.height;
            self.pickerBaseView.frame = temp;
        } completion:^(BOOL finished) {
            NSLog(@"picker hide");
            isPickerDisplay = NO;
        }];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)compone{
    
    
    if (pickerView.tag==1001) {
        
        pickerString=[pickerNames objectAtIndex:row];
                
        
    }else{
        
        pickerString=[pickerValues objectAtIndex:row];

    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{

    
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    
}



- (void)viewDidDisappear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];

}

- (IBAction)safeLocationRadius_btn:(id)sender
{
    
    _pickerView.tag=1002;
    
    [self.pickerView reloadAllComponents];
    [self hideShowPickerView];

}

- (IBAction)checkIntervalTime_btn:(id)sender
{
    _pickerView.tag=1001;

    [self.pickerView reloadAllComponents];
    [self hideShowPickerView];

}


- (IBAction)leaveSafeLocation_btn:(UIButton*)sender
{
    
    
    if ([sender.currentBackgroundImage isEqual:[UIImage imageNamed:@"unchecked white box"]]) {
        
        [_safelocationRadiusBtn setEnabled:YES];
        _safelocation_lbl.textColor = [UIColor whiteColor];
        _locationRadiusValue_lbl.textColor = [UIColor whiteColor];
        [_leaveSafeLocationBtn setBackgroundImage:[UIImage imageNamed:@"checked white Image"] forState:UIControlStateNormal];
        
        _distancelbl.textColor = [UIColor whiteColor];
        [_homeButton setEnabled:YES];
        [_homebuttonradious setEnabled:YES];
        [_overLapbutton setEnabled:YES];

    }
    else{
        
        
        
        [_safelocationRadiusBtn setEnabled:NO];
        _safelocation_lbl.textColor = [UIColor darkGrayColor];
        _locationRadiusValue_lbl.textColor = [UIColor darkGrayColor];
        [_leaveSafeLocationBtn setBackgroundImage:[UIImage imageNamed:@"unchecked white box"] forState:UIControlStateNormal];
        
        
        _distancelbl.textColor = [UIColor darkGrayColor];
        [_homeButton setEnabled:NO];
        [_homebuttonradious setEnabled:NO];
        [_overLapbutton setEnabled:NO];

    }

}



- (IBAction)emergencyEmail_btn:(UIButton*)sender
{
    
    if ([sender.currentBackgroundImage isEqual:[UIImage imageNamed:@"check Box Image"]]) {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
    }
    else{
        
        [sender setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];

    }
    
}

- (IBAction)emergencyText_btn:(UIButton*)sender
{
    
    if ([sender.currentBackgroundImage isEqual:[UIImage imageNamed:@"check Box Image"]]) {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
    }
    else{
        
        [sender setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];
        
    }
}

- (IBAction)emergencyCall_btn:(UIButton*)sender
{
    if ([sender.currentBackgroundImage isEqual:[UIImage imageNamed:@"check Box Image"]]) {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
    }
    else{
        
        [sender setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];
        
    }

}


- (IBAction)notificationViberate_btn:(UIButton*)sender
{
    if ([sender.currentBackgroundImage isEqual:[UIImage imageNamed:@"check Box Image"]]) {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
    }
    else{
        
        [sender setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];
        
    }
}

- (IBAction)notificationRingtone_btn:(UIButton*)sender
{
    if ([sender.currentBackgroundImage isEqual:[UIImage imageNamed:@"check Box Image"]]) {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
    }
    else{
        
        [sender setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];
        
    }

}


- (IBAction)shake_btn:(UIButton*)sender
{
    
    
    if ([sender.currentBackgroundImage isEqual:[UIImage imageNamed:@"check Box Image"]]) {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"shake_btn"];
    }
    else{
        
        [sender setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];
        
    }
}

- (IBAction)Recording_btn:(UIButton*)sender
{

    int tag=(int)sender.tag;
    
    switch (tag) {
        case 23:
            [_audio_btn setBackgroundImage:[UIImage imageNamed:@"Circle Filled Image"] forState:UIControlStateNormal];
            
            [_video_btn setBackgroundImage:[UIImage imageNamed:@"Circle Image"] forState:UIControlStateNormal];
            
            [_none_btn setBackgroundImage:[UIImage imageNamed:@"Circle Image"] forState:UIControlStateNormal];
            
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_audio_btn"];
           
            break;
        case 24:
            
            [_audio_btn setBackgroundImage:[UIImage imageNamed:@"Circle Image"] forState:UIControlStateNormal];
            
            [_video_btn setBackgroundImage:[UIImage imageNamed:@"Circle Filled Image"] forState:UIControlStateNormal];
            
            [_none_btn setBackgroundImage:[UIImage imageNamed:@"Circle Image"] forState:UIControlStateNormal];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_video_btn"];
            
            
            break;
        case 25:
            [_audio_btn setBackgroundImage:[UIImage imageNamed:@"Circle Image"] forState:UIControlStateNormal];
            
            [_video_btn setBackgroundImage:[UIImage imageNamed:@"Circle Image"] forState:UIControlStateNormal];
            
            [_none_btn setBackgroundImage:[UIImage imageNamed:@"Circle Filled Image"] forState:UIControlStateNormal];
           [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_none_btn"];
            break;
            
        default:
            break;
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)cancelButtonActon:(id)sender {
    
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect temp = self.pickerBaseView.frame;
        temp.origin.y = self.view.frame.size.height;
        self.pickerBaseView.frame = temp;
    } completion:^(BOOL finished) {
        NSLog(@"picker hide");
        isPickerDisplay = NO;
    }];

}
- (IBAction)doneButtonAction:(id)sender {

    if (_pickerView.tag==1001) {
        
        _setlocationValue_lbl.text=pickerString;
//        if (your_float1 <= 10.00)
//        {
       float your_float = [_setlocationValue_lbl.text floatValue];
        
//        if (your_float <= 1.00)
//        {
               CIBScallingViewController *CIBScalling = [self.storyboard instantiateViewControllerWithIdentifier:@"CIBScallingViewController"];

            [self.navigationController pushViewController:CIBScalling animated:NO];

//             _cibsCalling.hidden = true;
//        }
        
    }
    else{
        
        
        if ([pickerString isEqualToString:@"500 mts"]) {
            _distancelbl.text=@" 500 mts 1500 feet";
        }
        else  if ([pickerString isEqualToString:@"1 kms"])
            _distancelbl.text=@" 1000 mts 3000 feet";
        
        else if ([pickerString isEqualToString:@"2 kms"])
            _distancelbl.text=@" 2000 mts 6000 feet";
        
        else if ([pickerString isEqualToString:@"5 kms"])
            _distancelbl.text=@" 5000 mts 15000 feet";
        
        else if ([pickerString isEqualToString:@"10 kms"])
            _distancelbl.text=@" 10000 mts 30000 feet";
        
        
        _locationRadiusValue_lbl.text=pickerString;

        
    }
    
    

    [UIView animateWithDuration:0.25 animations:^{
        CGRect temp = self.pickerBaseView.frame;
        temp.origin.y = self.view.frame.size.height;
        self.pickerBaseView.frame = temp;
    } completion:^(BOOL finished) {
        NSLog(@"picker hide");
        isPickerDisplay = NO;
    }];

    
}
- (IBAction)saveAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_emergencyCall_btn"];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_emergencyText_btn"];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_emergencyEmail_btn"];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_notificationRingtone_btn"];
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"_notificationViberate_ttn"];
    NSLog(@"save data");
    // [self.view makeToast:@"Successfully saved" duration:3 position:CSToastPositionCenter];
}
@end
