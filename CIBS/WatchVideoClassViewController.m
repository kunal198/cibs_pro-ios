//
//  WatchVideoClassViewController.m
//  CIBS_PRO
//
//  Created by brst on 20/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "WatchVideoClassViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "AppDelegate.h"


@interface WatchVideoClassViewController ()
{AVPlayer *player;}

- (IBAction)nextClassAction:(UIButton *)sender;

@end




@implementation WatchVideoClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.topItem.title = @"Back";
    
     [self becomeFirstResponder];
//    NSLog(@">>>=%@",appDelegate.pathString);
    
    // remote file from server:
    NSURL *url = [NSURL fileURLWithPath:appDelegate.pathString];
    
    // create a player view controller
    player = [AVPlayer playerWithURL:url];
    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
    
    [self addChildViewController:controller];
    [self.view addSubview:controller.view];
    
    controller.view.frame = CGRectMake(self.view.frame.origin.x+10 , self.view.center.y-140,self.view.frame.size.width-20,  280);
    controller.player = player;
    controller.showsPlaybackControls = YES;
    player.closedCaptionDisplayEnabled = NO;
    [player pause];
    [player play];// remote file from server:
    
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (event.type == UIEventSubtypeMotionShake) {
        NSLog(@"Shake is detected");
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationItem.title = @"Watch Video";
    
}


- (void)viewDidDisappear:(BOOL)animated{

    [player pause];
    
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)nextClassAction:(UIButton *)sender {

    [self performSegueWithIdentifier:@"VideoToUserEdit" sender:self];

}
@end
