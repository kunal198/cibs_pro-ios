//
//  InstructionViewController.m
//  CIBS
//
//  Created by brst on 1/21/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

#import "InstructionViewController.h"
#import "SWRevealViewController.h"
@interface InstructionViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancel;

@end

@implementation InstructionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//                UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 568, 568)];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.cancel setTarget: self.revealViewController];
        [self.cancel setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

                NSURL *targetURL = [[NSBundle mainBundle] URLForResource:@"CIBS_Instructions_pro_v1" withExtension:@"pdf"];
                NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
                [_webview loadRequest:request];
                [self.view addSubview:_webview];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
