//
//  AppDelegate.m
//  CIBS_PRO
//
//  Created by Brst on 12/7/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "SidebarTableViewController.h"
#import "ViewController.h"
//#import "LocationViewController.h"

#import "AddContactVC.h"
#import "CameraVewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
#define DataName @"story.mp4"

/*
#import "UIView+Toast.h"
#import "AppDelegate.h"

#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#endif /* PrefixHeader_pch
*/


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _arrayofcontact=[[NSMutableArray alloc] init];
    _LocationsArray=[[NSMutableArray alloc] init];
    
    
    [self CopyDatabase];
    
     [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"presetSettings"];
    
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"showRegister"])
    {
        
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        ViewController *ViewC = (ViewController*)[story instantiateViewControllerWithIdentifier: @"ViewController"];
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:ViewC];
        SidebarTableViewController *rearViewController = (SidebarTableViewController*)[story instantiateViewControllerWithIdentifier: @"SidebarTableViewController"];
        
        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
        
        mainRevealController.rearViewController = rearViewController;
        mainRevealController.frontViewController= frontNavigationController;
        self.window.rootViewController =nil;
        self.window.rootViewController = mainRevealController;
        [self.window makeKeyAndVisible];
        
        
    }
    
       
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"addcontact"])
    {
        
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        AddContactVC *AddContactC = (AddContactVC*)[story instantiateViewControllerWithIdentifier: @"AddContactVC"];
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:AddContactC];
        SidebarTableViewController *rearViewController = (SidebarTableViewController*)[story instantiateViewControllerWithIdentifier: @"SidebarTableViewController"];
        
        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
        
        mainRevealController.rearViewController = rearViewController;
        mainRevealController.frontViewController= frontNavigationController;
        self.window.rootViewController =nil;
        self.window.rootViewController = mainRevealController;
        [self.window makeKeyAndVisible];
        
        
    }
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"cameraSave"])
    {
        
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        CameraVewController * CameraC = (CameraVewController*)[story instantiateViewControllerWithIdentifier: @"CameraVewController"];
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:CameraC];
        SidebarTableViewController *rearViewController = (SidebarTableViewController*)[story instantiateViewControllerWithIdentifier: @"SidebarTableViewController"];
        
        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
        
        mainRevealController.rearViewController = rearViewController;
        mainRevealController.frontViewController= frontNavigationController;
        self.window.rootViewController =nil;
        self.window.rootViewController = mainRevealController;
        [self.window makeKeyAndVisible];
        
        
    }

    if([[NSUserDefaults standardUserDefaults] boolForKey:@"datasaved"])
    {
        
        
        UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        HomeViewController *descController = (HomeViewController*)[st instantiateViewControllerWithIdentifier: @"HomeViewController"];
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
        SidebarTableViewController *rearViewController = (SidebarTableViewController*)[st instantiateViewControllerWithIdentifier: @"SidebarTableViewController"];
        
        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
        
        mainRevealController.rearViewController = rearViewController;
        mainRevealController.frontViewController= frontNavigationController;
        self.window.rootViewController =nil;
        self.window.rootViewController = mainRevealController;
        [self.window makeKeyAndVisible];
        
        
    }

    //file:///private/var/mobile/Containers/Data/Application/FD2640F0-66F2-405C-9E45-AD41ABABD46E/tmp/50419412445__D8A76490-947D-4E51-BF78-8B991F1EF2BE.MOV
    
    
    
    return YES;
}


- (NSString*)filepath
{
    NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *DocumentDir = [Paths objectAtIndex:0];
    //NSLog(@"%@",DocumentDir);
    return [DocumentDir stringByAppendingPathComponent:DataName];
    
}

- (void)CopyDatabase
{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:[self filepath]];
    NSString *FileDB = [[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:DataName];
    
    _pathString=[FileDB copy];
    if (success)
    {
        NSLog(@"File Exist");
        return;
    }
    else
    {
        
        
        
        [fileManager copyItemAtPath:FileDB toPath:[self filepath] error:nil];
    }
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"CIBS_PRO"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
