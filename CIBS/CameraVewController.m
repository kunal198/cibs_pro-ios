//
//  CameraVewController.m
//  CIBS_PRO
//
//  Created by mrinal khullar on 12/14/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "CameraVewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

#define URLname @"MusicVideo.mp4"


@interface CameraVewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{

    NSString *videoPhoto;

}
- (IBAction)action:(UIButton *)sender;
@property (retain, nonatomic) IBOutlet UIView *popUpView;
- (IBAction)closePopUpAction:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *cameraBtn;
@property (retain, nonatomic) IBOutlet UIButton *continueButton;
@property (retain, nonatomic) IBOutlet UIButton *infoButton;

@property (retain, nonatomic) MPMoviePlayerController *videoController;

@end

@implementation CameraVewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255.0 green:146.0/255.0 blue:63.0/255.0 alpha:1.0];
    

    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"cameraSave"];
    _popUpView.hidden=YES;

    _popUpView.layer.cornerRadius = 5;
    _popUpView.layer.masksToBounds = YES;

    
    self.navigationController.navigationBar.topItem.title = @"Back";

    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"Choose Photo";
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"media"] isEqualToString:@"video"]) {
       // self.videoController
        
        [self displayMediaPlayer];
    }
    else if([[NSUserDefaults standardUserDefaults]valueForKey:@"Image"]) {
    
        
        NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Image"];
        UIImage* image = [UIImage imageWithData:imageData];
        
        _image_view.image=image;
        _image_view.hidden=NO;

    }
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)closePopUpAction:(id)sender{
    
    _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    _popUpView.hidden=YES;
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  _popUpView.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];
    
    [_cameraBtn setEnabled:YES];
    [_continueButton setEnabled:YES];
    [_infoButton setEnabled:YES];

    
}


-(void)addSubviewWithBounce
{
    _popUpView.hidden=NO;
    _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [self.view bringSubviewToFront:_popUpView];
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  _popUpView.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];
    
    
    [_cameraBtn setEnabled:NO];
    [_continueButton setEnabled:NO];
    [_infoButton setEnabled:NO];

    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destination ViewController].
    // Pass the selected object to the new view controller.
}
*/


//-------------------------------------------BUTTON ACTIONS-----------------------------------------------------

- (IBAction)TakeSnapAction:(UIButton *)sender
{
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    } else
    {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
}
    
}


- (IBAction)LibraryAction{
    
    
   
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

            [self presentViewController:picker animated:YES completion:NULL];
    
    
    
}


-(void)displayMediaPlayer{
    
    if ([self.view.subviews containsObject:self.videoController.view]) {
        return;
    }

    
    _image_view.hidden=YES;
    self.videoController = [[MPMoviePlayerController alloc] init];
    
    NSLog(@"%@",[self filepath]);
    
    NSURL *url=[NSURL fileURLWithPath:[self filepath]];
    
    [self.videoController setContentURL:url];
    
    self.videoController.view.frame = CGRectMake(self.view.frame.origin.x+10 , self.view.center.y-140,self.view.frame.size.width-20,  280);

    
    [self.view addSubview:self.videoController.view];
    
    [self.videoController play];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"video" forKey:@"media"];

    
}


//----------------------------------------UIIMAGE PICKER CONTROLLER DELEGATES---------------------------------------

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if ([info[UIImagePickerControllerMediaType] isEqualToString:@"public.movie"]) {
        
        
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *tempPath = [documentsDirectory stringByAppendingFormat:@"/%@",URLname];
        BOOL success = [videoData writeToFile:tempPath atomically:NO];
        
         [self displayMediaPlayer];
        _image_view.hidden=YES;

        [picker dismissViewControllerAnimated:YES completion:NULL];

        [[NSUserDefaults standardUserDefaults] setValue:@"video" forKey:@"media"];

        
        
    }
    else{
        [[NSUserDefaults standardUserDefaults] setValue:@"photo" forKey:@"media"];

        if ([self.view.subviews containsObject:self.videoController.view] ) {
            
            [self.videoController.view removeFromSuperview];
        }
        
        
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.image_view.image = chosenImage;
        _image_view.hidden=NO;

            [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(self.image_view.image) forKey:@"Image"];
        
    [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)action:(UIButton *)sender {

    if (sender.tag==123) {
        
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select  option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"Take Photo",
                                @"Take Video",
                                @"Add Photo",
                                nil];
        popup.tag = 1;
        [popup showInView:self.view];

    }
    else if (sender.tag==124)
    {
        
        [self  performSegueWithIdentifier:@"CustomTimer" sender:self];
    }
    else{
    
        [self addSubviewWithBounce];
        
        //[self.view makeToast:@"Cooming Soon" duration:3 position:CSToastPositionCenter];

        
        
    }


}



- (NSString*)filepath
{
    NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *DocumentDir = [Paths objectAtIndex:0];
    //NSLog(@"%@",DocumentDir);
    return [DocumentDir stringByAppendingPathComponent:URLname];
    
}




- (IBAction)captureVideo:(id)sender {
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                
                case 0:
                    [self TakeSnapAction:nil];
                    break;
                
                case 1:{
                    
                    [self captureVideo:nil];
                    
                   // [self.view makeToast:@"Cooming Soon" duration:3 position:CSToastPositionCenter];
                }
                    break;
                case 2:
                    [self LibraryAction];
                    break;

                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}


@end
