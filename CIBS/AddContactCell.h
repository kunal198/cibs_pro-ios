//
//  AddContactCell.h
//  CIBS_PRO
//
//  Created by mrinal khullar on 12/14/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddContactCell : UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *label1;
@property (retain, nonatomic) IBOutlet UILabel *label2;
@property (retain, nonatomic) IBOutlet UIButton *deleteBtn;

@end
