//
//  SearchLocation.m
//  CIBS_PRO
//
//  Created by Brst on 12/7/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "SearchLocation.h"
#import <AddressBookUI/AddressBookUI.h>
@interface SearchLocation ()

@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property(nonatomic, retain) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UITextField *searchLocation;

- (IBAction)mapViewType:(id)sender;
- (IBAction)SearchAction:(id)sender;
- (IBAction)dismisButtton:(UIButton *)sender;
- (IBAction)savemyCurrentLocation:(id)sender;

@end

@implementation SearchLocation

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

- (void)viewDidLoad
{
    [super viewDidLoad];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    //   [_mapView addGestureRecognizer:tap];


    self.mapView.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [[self mapView] setShowsUserLocation:YES];
 
    
//    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance
//    (self.mapView.userLocation.coordinate, 2*1.145, 2*1.15);
//    [[self mapView] setRegion:viewRegion animated:YES];
    
    
    
    
    // Add this to solve a location authorization issue on iOS8
    
    // See more at: http://nevan.net/2014/09/core-location-manager-changes-in-ios-8/
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    
    self.mapView.showsUserLocation = YES;
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setZoomEnabled:YES];
    [self.mapView setScrollEnabled:YES];
    
    MKCoordinateRegion region;
    region.center.latitude = 25.03;
    region.center.longitude = 121.5;
    region.span.latitudeDelta = 0.2;
    region.span.longitudeDelta = 0.2;
    
    [self.mapView setRegion:region animated:YES];
    
    
    
    _search_TxtField.layer.borderWidth = 2.0f;
    _search_TxtField.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:1.0/255.0 alpha:1.0].CGColor;
    [_search_TxtField.layer setCornerRadius:8.0f];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.locationManager stopUpdatingLocation];
}

//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation*)userLocation
//{
//    double X= userLocation.coordinate.latitude;
//    double Y= userLocation.coordinate.longitude;
//    
//    NSLog(@"%f,%f",X,Y);
//    
//    CLLocationCoordinate2D currentPosition = [userLocation coordinate];
//    MKCoordinateRegion region =MKCoordinateRegionMakeWithDistance(currentPosition, 800, 800);
//    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
//}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    //NSLog(@"%@", [self deviceLocation]);
    
    //View Area
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = self.locationManager.location.coordinate.latitude;
    region.center.longitude = self.locationManager.location.coordinate.longitude;
    region.span.longitudeDelta = 0.005f;
    region.span.longitudeDelta = 0.005f;
    [_mapView setRegion:region animated:YES];
    
}

#pragma mark -
#pragma mark MapViewDelegate

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation
{
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
    
    [_mapView removeAnnotations:[_mapView annotations]];
    
    // add the annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = aUserLocation.coordinate;
    
    
    
    [self.mapView addAnnotation:point];
    
    
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:aUserLocation.coordinate.latitude longitude:aUserLocation.coordinate.longitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  
                  if (placemark)
                  {
                      
                      NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      
                      NSLog(@"placemark %@",placemark.region);
                      NSLog(@"placemark %@",placemark.country);  // Give Country Name
                      NSLog(@"placemark %@",placemark.locality); // Extract the city name
                      NSLog(@"location %@",placemark.name);
                      NSLog(@"location %@",placemark.ocean);
                      NSLog(@"location %@",placemark.postalCode);
                      NSLog(@"location %@",placemark.subLocality);
                      
                      NSLog(@"location %@",placemark.location);
                      //Print the location to console
                      NSLog(@"I am currently at %@",locatedAt);
                      
                      point.title = locatedAt;
                      
                      MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                      point.coordinate = aUserLocation.coordinate;
                      point.title = placemark.country;
                      point.subtitle = placemark.name;
                      [self.mapView addAnnotation:point];
                      stringofuser = [locatedAt mutableCopy];
                      
                      // [_mapView.userLocation setTitle:locatedAt];
                      
                  }
                  else {
                      NSLog(@"Could not locate");
                  }
              }
     ];
    

 

}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation> )annotation
{
    
    
    if (annotation == mapView.userLocation)
    {
        return nil;
    }
    
    MKPinAnnotationView *pinView = nil;
    
    
   
    
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         MKAnnotationView *ulv = [mapView viewForAnnotation:mapView.userLocation];

                         ulv.hidden = YES;
                     }];
    

    
    
    static NSString *defaultPinID = @"identifier";
    pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if ( pinView == nil )
    {
        NSLog(@"Inside IF");
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        pinView.pinColor = MKPinAnnotationColorRed;  //or Green or Purple
   //     pinView.
        pinView.enabled = YES;
        pinView.canShowCallout = YES;
        
      UIButton *btn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
//        Accessoryview for the annotation view in ios.
        pinView.rightCalloutAccessoryView = btn;
        
        
        
        CLGeocoder *ceo = [[CLGeocoder alloc]init];
        
       
        
        
   //insert your coordinates
        
        CLLocation *loc = [[CLLocation alloc]initWithLatitude:annotation.coordinate.latitude longitude:annotation.coordinate.longitude];
        
        

        
        [ceo reverseGeocodeLocation:loc
                  completionHandler:^(NSArray *placemarks, NSError *error) {
                      CLPlacemark *placemark = [placemarks objectAtIndex:0];
                      
                      if (placemark) {
                          
                          NSLog(@"placemark %@",placemark);
                          //String to hold address
                          NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                          NSLog(@"addressDictionary %@", placemark.addressDictionary);
                          
                          NSLog(@"placemark %@",placemark.region);
                          NSLog(@"placemark %@",placemark.country);  // Give Country Name
                          NSLog(@"placemark %@",placemark.locality); // Extract the city name
                          NSLog(@"location %@",placemark.name);
                          NSLog(@"location %@",placemark.ocean);
                          NSLog(@"location %@",placemark.postalCode);
                          NSLog(@"location %@",placemark.subLocality);
                          
                          NSLog(@"location %@",placemark.location);
                          //Print the location to console
                          NSLog(@"I am currently at %@",locatedAt);
                          
                     //     annotation.title = locatedAt;
                          
                          
                      }
                      else {
                          NSLog(@"Could not locate");
                      }
                  }
         ];
        
        
        
    }
    else
    {
        [UIView animateWithDuration:0.3f
                         animations:^{
                             pinView.annotation = annotation;
                         }];
       
    }
    
    return pinView;
}

#pragma mark -
#pragma mark Text_Field


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_search_TxtField resignFirstResponder];

    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}


- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}
- (NSString *)deviceLat {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
}
- (NSString *)deviceLon {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
}
- (NSString *)deviceAlt {
    return [NSString stringWithFormat:@"%f", self.locationManager.location.altitude];
}



/*
- (MKAnnotationView *) mapView: (MKMapView *) mapView viewForAnnotation:(id<MKAnnotation>) annotation
{
    if (annotation == mapView.userLocation)
    {
        return nil;
    }
    else
    {
        MKAnnotationView *ulv = [mapView viewForAnnotation:mapView.userLocation];
        ulv.hidden = YES;
        
        MKAnnotationView *pin = (MKAnnotationView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier: @"VoteSpotPin"];
        if ( pin == nil )
        {
            NSLog(@"Inside IF");
            pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
            
            pin.pinColor = MKPinAnnotationColorRed;  //or Green or Purple
            
            pin.enabled = YES;
            pin.canShowCallout = YES;
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            
            //Accessoryview for the annotation view in ios.
            pin.rightCalloutAccessoryView = btn;
        }
        else
        {
            pinView.annotation = annotation;
        }
        
//        [pin setImage:[UIImage imageNamed:@"user_location_new.png"]];
//        pin.canShowCallout = YES;
//        pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
       
        return pin;
    }
}
*/


#pragma mark -
#pragma mark TableDelegates

- (IBAction)mapViewType:(UIButton*)sender {
    
    int type= (int)sender.tag;
 
    switch (type) {
        case 1:
            _mapView.mapType=MKMapTypeStandard;
            break;
            
        case 2:
            _mapView.mapType=MKMapTypeSatellite;

            break;
            
        case 3:
            _mapView.mapType=MKMapTypeHybrid;
            break;
            
        default:
            break;
    }
    
}
- (void)reverseGeocode:(CLLocation *)location {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
//            self.myAddress.text = [NSString stringWithFormat:@"%d", ABCreateStringWithAddressDictionary(placemark.addressDictionary, NO)];
        }
    }];
}
- (IBAction)SearchAction:(id)sender {
    
        NSString *location = _search_TxtField.text;
//    [self getLocationFromAddressString:location];
//        [self reverseGeocode:location];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:location
                 completionHandler:^(NSArray* placemarks, NSError* error)
     {
         
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         if (placemarks && placemarks.count > 0) {
             
             NSLog(@"placemark %@",[placemark.addressDictionary valueForKey:@"FormattedAddressLines"]);
             
             CLPlacemark *topResult = [placemarks objectAtIndex:0];
             MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
             
             [_mapView removeAnnotations:[_mapView annotations]];
             
             CLLocationCoordinate2D location;
             location.latitude = placemark.location.coordinate.latitude;
             location.longitude = placemark.location.coordinate.longitude;
             
             
             MKCoordinateRegion region = self.mapView.region;
             region.center = location;
             region.span.longitudeDelta /= 5.0;
             region.span.latitudeDelta /= 5.0;
             
             [_mapView setRegion:region animated:YES];
             [self.mapView addAnnotation:placemark];
             
             [_search_TxtField resignFirstResponder];
             
             NSMutableArray *arr = [placemark.addressDictionary valueForKey:@"FormattedAddressLines"];
             //[[array valueForKey:@"description"] componentsJoinedByString:@""];
             NSString *str = [arr  componentsJoinedByString:@","];
             stringofuser = [str mutableCopy];
             
             
         }
         
     }
     
     ];
//
    
}
//-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
//    double latitude = 0, longitude = 0;
//    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
//    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
//    if (result) {
//        NSScanner *scanner = [NSScanner scannerWithString:result];
//        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
//            [scanner scanDouble:&latitude];
//            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
//                [scanner scanDouble:&longitude];
//            }
//        }
//    }
//    CLLocationCoordinate2D center;
//    center.latitude=latitude;
//    center.longitude = longitude;
//    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
//    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
//    
//    
//
//    return center;
//    
//}

- (IBAction)dismisButtton:(UIButton *)sender{

    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)savemyCurrentLocation:(id)sender {
    
    if([stringofuser isKindOfClass:[NSNull class]]||stringofuser==nil||[stringofuser isEqualToString:@""]){
        
        [self.view makeToast:@"Please select Location first" duration:3 position:CSToastPositionCenter];
        return;
    }
    
    [appDelegate.LocationsArray addObject:stringofuser];
    appDelegate.arratofLocation = stringofuser;
    
    [self.view makeToast:@"Location added successfully." duration:3 position:CSToastPositionCenter];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


- (IBAction)setSelectedAction:(UIButton *)sender
{
    
    if([stringofuser isKindOfClass:[NSNull class]]||stringofuser==nil||[stringofuser isEqualToString:@""]){
        
        [self.view makeToast:@"Please select Location first" duration:3 position:CSToastPositionCenter];
        return;
    }
    
    [appDelegate.LocationsArray addObject:stringofuser];
    appDelegate.arratofLocation = stringofuser;
    
    [self.view makeToast:@"Location added successfully." duration:3 position:CSToastPositionCenter];

    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
