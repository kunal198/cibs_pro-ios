//

//  CIBS_PRO
//
//  Created by brst on 14/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface UpdateLocationCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *LocNameLbl;
//- (IBAction)RemoveButton:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *crossButton;
@property (retain, nonatomic) IBOutlet UpdateLocationCell *view;

@end
@implementation UpdateLocationCell
@end

@interface UpdateLocationViewController : UIViewController
@property (retain, nonatomic) IBOutlet UITableView *table_view;

@end
