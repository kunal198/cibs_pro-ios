//
//  AddContactVC.m
//  CIBS_PRO
//
//  Created by mrinal khullar on 12/14/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "AddContactVC.h"
#import "AddContactCell.h"

@interface AddContactVC ()
@property (retain, nonatomic) IBOutlet UIView *CustomContactbaseView;
- (IBAction)addCancelActionBV:(id)sender;
- (IBAction)saveContactAction:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *addcontactBtn;

@property (retain, nonatomic) IBOutlet UIButton *addmanuallyBtn;
@property (retain, nonatomic) IBOutlet UIButton *addContactBtn;
@property (retain, nonatomic) IBOutlet UIButton *continueBtn;
@property (retain, nonatomic) IBOutlet UIButton *Skip;
@property (retain, nonatomic) IBOutlet UIButton *sacvebutton;


@property (retain, nonatomic) IBOutlet UIView *popUpView;
- (IBAction)closePopUpAction:(id)sender;


- (IBAction)infobutton:(id)sender;

@end

@implementation AddContactVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255.0 green:146.0/255.0 blue:63.0/255.0 alpha:1.0];
    

    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"addcontact"];
    arrayoflist = [[NSMutableArray alloc] init];
    _table_view.hidden = YES;
    _popUpView.hidden=YES;

    
    _popUpView.layer.cornerRadius = 5;
    _popUpView.layer.masksToBounds = YES;

    
    self.navigationController.navigationBar.topItem.title = @"Back";
    
    
//    _addcontactBtn.titleLabel.text=@"Add manually";
//    _addcontactBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    _addcontactBtn.titleLabel.numberOfLines = 2;//if you want unlimited number of lines put 0
//    _addcontactBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    _addmanuallyBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _addmanuallyBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_addmanuallyBtn setTitle: @"Add \nManually" forState: UIControlStateNormal];

    
    _addContactBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _addContactBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_addContactBtn setTitle: @"Add \nContact" forState: UIControlStateNormal];
    
    
    _continueBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _continueBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_continueBtn setTitle: @"Continue" forState: UIControlStateNormal];
    
    _Skip.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _Skip.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_Skip setTitle: @"Screen \ninfo" forState: UIControlStateNormal];
    
    // Email Validations
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];

   UITapGestureRecognizer * tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [_CustomContactbaseView addGestureRecognizer:tapper];


    boolforarray = NO;
    
    _contactSubViewDetails.layer.cornerRadius = 5;
    _contactSubViewDetails.layer.masksToBounds = YES;
    _table_view.hidden = YES;
    
    _CustomContactbaseView.hidden=YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"Add Contacts";

    if (appDelegate.arrayofcontact.count>0) {
        
        _table_view.hidden = NO;
        [_table_view reloadData];
        
    }
    else{
        _table_view.hidden = YES;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [appDelegate.arrayofcontact count];    //count number of row from counting array hear cataGorry is An Array
}

- (IBAction)closePopUpAction:(id)sender{
    
    _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    _popUpView.hidden=YES;
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  _popUpView.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];
    
    [_addmanuallyBtn setEnabled:YES];
    [_addContactBtn setEnabled:YES];
    [_continueBtn setEnabled:YES];
    [_Skip setEnabled:YES];

    
}


-(void)addSubviewWithBounce
{
    _popUpView.hidden=NO;
    _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  _popUpView.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];
    
    
    
    [_addmanuallyBtn setEnabled:NO];
    [_addContactBtn setEnabled:NO];
    [_continueBtn setEnabled:NO];
    [_Skip setEnabled:NO];

}




- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell2";
    
    AddContactCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[AddContactCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:MyIdentifier];
    }
    
    
    _table_view.separatorColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.deleteBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(DeleteButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.label1.text = [appDelegate.arrayofcontact[indexPath.row] valueForKey:@"name"];
    
    if ([appDelegate.arrayofcontact[indexPath.row] valueForKey:@"Phone"] != nil)
    {
        
        cell.label2.text = [appDelegate.arrayofcontact[indexPath.row] valueForKey:@"Phone"];
        
    }
    
    else
    {
        
        if ([appDelegate.arrayofcontact[indexPath.row] valueForKey:@"email"] != nil)
        {
            cell.label2.text = [appDelegate.arrayofcontact[indexPath.row] valueForKey:@"email"];
        }
        else
        {
            cell.label2.text = @"No contact info added";
        }
    }
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //[self AddManuallyAction:appDelegate.arrayofcontact[indexPath.row]];
    
    [self updatecontact:appDelegate.arrayofcontact[indexPath.row] IndexPath:indexPath];
    
    fromTable=YES;

}


- (IBAction)DeleteButton:(UIButton *)sender
{
    

    NSLog(@"DeleteButton%@",appDelegate.arrayofcontact);
    
    
    [appDelegate.arrayofcontact removeObjectAtIndex:sender.tag];
    
    [_table_view reloadData];
}



- (IBAction)ContactList_Action:(id)sender
{
    boolforarray = YES;
}




- (IBAction)Continue_Action:(id)sender
{
//    [self performSegueWithIdentifier:@"ContinnueAction" sender:self];
//
//    return;
    
    NSLog(@"%@",appDelegate.arrayofcontact);
    
    if (appDelegate.arrayofcontact.count>0) {
        
        [self performSegueWithIdentifier:@"ContinnueAction" sender:self];
    }
    else{
    
        [self.view makeToast:@"Please select contact first" duration:3 position:CSToastPositionCenter];
    }
    
}

//- (void)dealloc
//{
//  //  [_table_view release];
//
//    [_CustomContactbaseView release];
//    [_contactInfoView release];
//    [_contactUncerlineLbl release];
//    [_fullName_TxtField release];
//    [_fullName_underLineLbl release];
//    [_emailAddress_TxtField release];
//    [_email_underLine release];
//    [_phoneNumber_TxtField release];
//    [_phoneNumber_underline release];
//    [_contactInfoView release];
//    [_contactSubViewDetails release];
//    [super dealloc];
//}
//


-(void)updatecontact:(NSMutableDictionary*)dict IndexPath:(NSIndexPath *)indexPath{

    
    
    if ([dict valueForKey:@"Phone"]) {
        _phoneNumber_TxtField.text=[dict valueForKey:@"Phone"];
    }
    if ([dict valueForKey:@"name"]) {
        _fullName_TxtField.text=[dict valueForKey:@"name"];
    }
    if ([dict valueForKey:@"email"]) {
        _emailAddress_TxtField.text=[dict valueForKey:@"name"];
    }
    
    _sacvebutton.tag=indexPath.row;

    if (_CustomContactbaseView.hidden) {
        _CustomContactbaseView.alpha = 0;
        _CustomContactbaseView.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            _CustomContactbaseView.alpha = 1;
        }];
    }
    else{
        
        
        //_CustomContactbaseView.alpha = 1;
        
        [UIView animateWithDuration:2.0f animations:^{
            
            [_CustomContactbaseView setAlpha:0.0f];
            [_CustomContactbaseView setHidden:YES];
        } completion:nil];
        
        
    }
    
    
}


- (IBAction)AddManuallyAction:(id)sender {
   
    _phoneNumber_TxtField.text=@"";
    _fullName_TxtField.text=@"";
    _emailAddress_TxtField.text=@"";

    
    
    if (_CustomContactbaseView.hidden) {
            _CustomContactbaseView.alpha = 0;
            _CustomContactbaseView.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                _CustomContactbaseView.alpha = 1;
            }];
    }
    else{
    
    
            //_CustomContactbaseView.alpha = 1;
        
        [UIView animateWithDuration:2.0f animations:^{
            
                    [_CustomContactbaseView setAlpha:0.0f];
                    [_CustomContactbaseView setHidden:YES];
            } completion:nil];
        

    }
    
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    if(textField.returnKeyType==UIReturnKeyNext)
//    {
//        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
//        [next becomeFirstResponder];
//    }
//    else if (textField.returnKeyType==UIReturnKeyDone)
    //{
    [textField endEditing:true];
        [textField resignFirstResponder];
   // }
    return YES;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



- (IBAction)addCancelActionBV:(id)sender
{
    [_CustomContactbaseView setHidden:YES];
    
    _fullName_TxtField.text=@"";
    _emailAddress_TxtField.text=@"";
    _phoneNumber_TxtField.text=@"";

    fromTable=NO;

}


- (IBAction)saveContactAction:(UIButton*)sender {
    
    if (fromTable) {
    
    NSIndexPath *selectedIndexPath = [self.table_view indexPathForSelectedRow];
    UITableViewCell *swipedCell  = [self.table_view cellForRowAtIndexPath:selectedIndexPath];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    
    
    UILabel *lbl=[swipedCell.contentView viewWithTag:22];
    
    if (_fullName_TxtField.text.length>0) {
        lbl.text=_fullName_TxtField.text;
        [dict setValue:_fullName_TxtField.text forKey:@"name"];
    }
    
    
    UILabel *lbl1=[swipedCell.contentView viewWithTag:24];

    if (_phoneNumber_TxtField.text.length>0) {
        lbl1.text=_phoneNumber_TxtField.text;
        [dict setValue:_phoneNumber_TxtField.text forKey:@"Phone"];

    }

    UILabel *lbl21=[swipedCell.contentView viewWithTag:23];
    
    if (_emailAddress_TxtField.text.length>0) {
        lbl21.text=_emailAddress_TxtField.text;
        [dict setValue:_emailAddress_TxtField.text forKey:@"email"];
    }

    [appDelegate.arrayofcontact replaceObjectAtIndex:selectedIndexPath.row withObject:dict];

        fromTable=NO;
        
        [_table_view reloadData];
        
        _CustomContactbaseView.hidden=YES;
        _fullName_TxtField.text=@"";
        _emailAddress_TxtField.text=@"";
        _phoneNumber_TxtField.text=@"";

        
        return;
    }
    
    
    // Email Validations
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];

    if((_fullName_TxtField.text.length==0)||(_phoneNumber_TxtField.text.length==0))
    {
        
        
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"CIBS"
                                                              message:@"Please enter all fields"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];

        
    }
    
    else if (!_phoneNumber_TxtField.text.length>0){
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"CIBS"
                                                              message:@"Please enter Mobile no"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        return;
        
    }
    
    else
    {
        
        _table_view.hidden=NO;
        
        fullName = _fullName_TxtField.text;
        emailAddress = _emailAddress_TxtField.text;
        phoneNumber = _phoneNumber_TxtField.text;
        
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:phoneNumber, @"Phone", emailAddress, @"email",fullName, @"name", nil];
        
        [appDelegate.arrayofcontact addObject:dict];
        
        [_table_view reloadData];
        
        _CustomContactbaseView.hidden=YES;
        _fullName_TxtField.text=@"";
        _emailAddress_TxtField.text=@"";
        _phoneNumber_TxtField.text=@"";
        
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

     if (textField.tag==4)
    {
        if ([textField.text length]<=9 ||[string isEqualToString:@""])
        {
            //            /*  limit to only numeric characters  */
            //
            //            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            //            for (int i = 0; i < [string length]; i++) {
            //                unichar c = [string characterAtIndex:i];
            //                if ([myCharSet characterIsMember:c]) {
            //                    return YES;
            //                }
            //            }
            
            return YES;
            
            }
        else{
        
            return NO;
        }
    }
    
    return YES;
    
}

- (IBAction)infobutton:(id)sender {
  
    [self addSubviewWithBounce];
    
//    [self.view makeToast:@"Cooming Soon" duration:3 position:CSToastPositionCenter];

}
@end
