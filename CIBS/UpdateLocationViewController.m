//

//  CIBS_PRO
//
//  Created by brst on 14/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "UpdateLocationViewController.h"
#import "SWRevealViewController.h"
@interface UpdateLocationViewController ()
@property (retain, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)ButtonActions:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *addlocationBurtton;
@property (retain, nonatomic) IBOutlet UIButton *SkipButton;
@property (retain, nonatomic) IBOutlet UIButton *infoButton;
@property (retain, nonatomic) IBOutlet UIView *popUpView;
- (IBAction)closePopUpAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end

@implementation UpdateLocationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

    _table_view.hidden = YES;
    
    
    _popUpView.layer.cornerRadius = 5;
    _popUpView.layer.masksToBounds = YES;
    
    
    _popUpView.hidden=YES;

    
//    self.navigationController.navigationBar.topItem.title = @"Back";
    

    
    _addlocationBurtton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _addlocationBurtton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_addlocationBurtton setTitle: @"Add \nLocation" forState: UIControlStateNormal];
    
    
    _SkipButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _SkipButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_SkipButton setTitle: @"Skip \nContinue" forState: UIControlStateNormal];

    
    _infoButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _infoButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_infoButton setTitle: @"Screen \ninfo" forState: UIControlStateNormal];

}


- (IBAction)closePopUpAction:(id)sender{

    _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    _popUpView.hidden=YES;
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  _popUpView.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];
    
    [_addlocationBurtton setEnabled:YES];
    [_SkipButton setEnabled:YES];
    [_infoButton setEnabled:YES];

    
}


-(void)addSubviewWithBounce
{
    _popUpView.hidden=NO;
    _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
      [_addlocationBurtton setEnabled:NO];
      [_SkipButton setEnabled:NO];
      [_infoButton setEnabled:NO];

    [UIView animateWithDuration:0.3/1.5 animations:^{
        
        _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
        
    }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.3/2 animations:^{
                             
                             _popUpView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                             
                         }
                                          completion:^(BOOL finished) {
                                              
                                              [UIView animateWithDuration:0.3/2 animations:^{
                                                  _popUpView.transform = CGAffineTransformIdentity;
                                                  
                                              }];
                                          }];
                     }];
}


-(void)viewWillAppear:(BOOL)animated
{
    
//    self.navigationItem.title = @"Add Location";

    NSLog(@"abcd %@", appDelegate.LocationsArray);
  
    if(!(appDelegate.LocationsArray.count> 0))
    {        _table_view.hidden = YES; }
    
    else
    {
        _table_view.hidden = NO;
        
        [self.table_view reloadData];

        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return appDelegate.LocationsArray.count;    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView   cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"identifier";
    
    UpdateLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    _table_view.separatorColor = [UIColor clearColor];
    
    if (cell == nil)
    {
        cell = [[UpdateLocationCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.crossButton.tag=indexPath.row;
    
    cell.LocNameLbl.text=[appDelegate.LocationsArray objectAtIndex:indexPath.row];
    
    return cell;
    
}





- (IBAction)Delete_Action:(UIButton *)sender
{
    
    
    [appDelegate.LocationsArray removeObjectAtIndex:sender.tag];
    
    [_table_view reloadData];

    
    return;
    
    
    NSLog(@"appDelegate.LocationsArray=%@",appDelegate.LocationsArray);
    
    int row =[sender.titleLabel.text intValue];
    [appDelegate.LocationsArray removeObjectAtIndex:row];

    NSLog(@"appDelegate.LocationsArray=%@",appDelegate.LocationsArray);

    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    

    if (!(appDelegate.LocationsArray.count>0)) {
            _table_view.hidden=YES;

    }else{
        [_tableView reloadData];
        _table_view.hidden=NO;

    }
    

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/








- (IBAction)ButtonActions:(UIButton*)sender {
    
    if (sender.tag==12) {
        
        [self performSegueWithIdentifier:@"ContactClass" sender:self];
        
    }else{
    
        [self addSubviewWithBounce];
      //  [self.view makeToast:@"Cooming Soon" duration:3 position:CSToastPositionCenter];

    }
}
@end
