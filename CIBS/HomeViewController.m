//
//  HomeViewController.m
//  CIBS
//
//  Created by Brst on 12/28/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "PopUpViewController.h"


@interface HomeViewController ()
{
    UIAlertAction* okAction;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *cibsCalling;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
- (IBAction)checkIn:(id)sender;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"datasaved"];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.0/255.0 green:146.0/255.0 blue:63.0/255.0 alpha:1.0];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    _cibsCalling.hidden = true;

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)checkIn:(id)sender {
    
//    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your password to view the list" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
//    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
//    [alert show];
//    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""message:@"Please enter your password to view the list"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    //            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
    //             {
    //                 textField.placeholder = NSLocalizedString(@"LoginPlaceholder", @"Login");
    //                 [textField addTarget:self
    //                               action:@selector(alertTextFieldDidChange:)
    //                     forControlEvents:UIControlEventEditingChanged];
    //             }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"PasswordPlaceholder", @"Password");
         textField.secureTextEntry = YES;
     }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action)
                               {
                                   UITextField *password = alertController.textFields.firstObject;
                                   NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                   NSString *password2 = [prefs stringForKey:@"password"];
                                   if (password2 == password.text)
                                   {
//                                       [self performSegueWithIdentifier:@"EmergencyContacts" sender:self];
                                   }
                                   else
                                   {
                                       [self.view makeToast:@"Please enter correct password." duration:3 position:CSToastPositionCenter];
                                       
                                       
                                   }
                                   
                                   
                                   NSLog(@"OK action");
                               }];
    
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
//    UIAlertController * alertCont=   [UIAlertController
//                                  alertControllerWithTitle:@""
//                                  message:@"Please enter your password to view the list"
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    
//    [alertCont addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
//        textField.placeholder = @"Enter Password";
//        textField.secureTextEntry = YES;
//        textField.delegate=self;
//    }];
//    
//    okAction = [UIAlertAction
//                         actionWithTitle:@"OK"
//                         style:UIAlertActionStyleDefault
//                         handler:^(UIAlertAction * action)
//                         {
//                            // [alertCont dismissViewControllerAnimated:YES completion:nil];
//                         
//                         
//                         }];
//    
//    UIAlertAction* cancel = [UIAlertAction
//                             actionWithTitle:@"Cancel"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 //[alertCont dismissViewControllerAnimated:YES completion:nil];
//                             }];
//
//    
//    
//    [alertCont addAction:okAction];
//    [alertCont addAction:cancel];
//    
//    
//    [self presentViewController:alertCont animated:YES completion:nil];
    
//    [okAction setEnabled:NO];

    
//    PopUpViewController *objPopUpViewController=[[PopUpViewController alloc] init];
//    [objPopUpViewController showInView:self.view animated:YES];
}
- (IBAction)sendAlertBtn:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert title" message:@"CIBS Notification Alert Activated" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];

}

- (IBAction)deactivateBtn:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert title" message:@"CIBS Notification Alert Deactivated" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];

}
- (IBAction)whereAmBtn:(id)sender {
}


#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *finalString = [textField.text stringByReplacingCharactersInRange:range withString:string];
 
    if ([textField.text length]<=3)
    {
        if ([finalString length]==4) {
            
            [self showMessage:@"Please enter vaild password" atPoint:CGPointMake(100  , 100)];
            
            [okAction setEnabled:(finalString.length == 4)];  }
        else{
            [okAction setEnabled:NO];
        }
        
        return YES;
    }
    else if([@"" isEqualToString:string])
    {
        textField.text=[textField.text substringToIndex:3 ];
        [okAction setEnabled:NO];

    }
    
    return NO;

    

}


- (void)showMessage:(NSString*)message atPoint:(CGPoint)point {
    const CGFloat fontSize = 16;
    
    
    UILabel* label = [[UILabel alloc] init];
    
    label.backgroundColor = [UIColor darkGrayColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:fontSize];
    label.text = message;
    label.layer.masksToBounds = true;
    label.layer.cornerRadius = 5;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    [label setFont:[UIFont systemFontOfSize:16]];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    label.frame=CGRectMake(0, 100, 180, 45);
    
    [label setFrame:CGRectMake(((self.view.frame.size.width/2.0f) - (label.frame.size.width/2.0f)), 100, 180, 45)];
    [appDelegate.window addSubview:label];
    
    [UIView animateWithDuration:0.3 delay:2 options:0 animations:^{
        label.alpha = 0;
    } completion:^(BOOL finished) {
        label.hidden = YES;
        [label removeFromSuperview];
    }];
}


@end
