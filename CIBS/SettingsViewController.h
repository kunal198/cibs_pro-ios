//
//  SettingsViewController.h
//  CIBS_PRO
//
//  Created by brst on 14/12/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController    <UIPickerViewDataSource, UIPickerViewDelegate,UITextViewDelegate>

@property (retain, nonatomic) IBOutlet UILabel *locationRadiusValue_lbl;
@property (strong, nonatomic) IBOutlet UIButton *checkIntervalTimeBtn;
@property (strong, nonatomic) IBOutlet UIButton *leaveSafeLocationBtn;
@property (strong, nonatomic) IBOutlet UIButton *safelocationRadiusBtn;
@property (strong, nonatomic) IBOutlet UIButton *emergencyEmail_btn;
@property (strong, nonatomic) IBOutlet UIButton *emergencyText_btn;
@property (strong, nonatomic) IBOutlet UIButton *emergencyCall_btn;
@property (strong, nonatomic) IBOutlet UIButton *notificationViberate_ttn;
@property (strong, nonatomic) IBOutlet UIButton *notificationRingtone_btn;
@property (strong, nonatomic) IBOutlet UIButton *shakeBtn;
@property (strong, nonatomic) IBOutlet UIButton *audio_btn;
@property (strong, nonatomic) IBOutlet UIButton *video_btn;
@property (strong, nonatomic) IBOutlet UIButton *none_btn;
@property (strong, nonatomic) IBOutlet UITextView *personalMessage_TxtView;
@property (retain, nonatomic) IBOutlet UILabel *setlocationValue_lbl;
@property (retain, nonatomic) IBOutlet UILabel *safelocation_lbl;

@property (retain, nonatomic) IBOutlet UIView *scrollView;



- (IBAction)checkIntervalTime_btn:(id)sender;
- (IBAction)leaveSafeLocation_btn:(id)sender;
- (IBAction)safeLocationRadius_btn:(id)sender;
- (IBAction)emergencyEmail_btn:(id)sender;
- (IBAction)emergencyText_btn:(id)sender;
- (IBAction)emergencyCall_btn:(id)sender;
- (IBAction)notificationViberate_btn:(id)sender;
- (IBAction)notificationRingtone_btn:(id)sender;
- (IBAction)shake_btn:(id)sender;
- (IBAction)audionRecording_btn:(id)sender;
- (IBAction)videoRecording_btn:(id)sender;
- (IBAction)none_btn:(id)sender;


@end
