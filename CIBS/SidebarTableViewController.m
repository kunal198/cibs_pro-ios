//
//  SidebarTableViewController.m
//  SidebarDemo
//
//  Created by Simon Ng on 10/11/14.
//  Copyright (c) 2014 AppCoda. All rights reserved.
//

#import "SidebarTableViewController.h"
#import "SWRevealViewController.h"

@interface SidebarTableViewController ()<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SidebarTableViewController {
    NSArray *menuItems;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuItems = @[@"news1", @"news2",@"news10", @"news3", @"news4", @"news5", @"news6", @"news7", @"news8", @"news9"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return menuItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
//    cell.highlightedTextColor = [UIColor colorWithRed:0.0/255.0 green:144.0/255.0 blue:71.0/255.0 alpha:0.5];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"HomeScreen" sender:self];

            break;
        case 1:
            [self performSegueWithIdentifier:@"profile" sender:self];
//            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
            break;
        case 2:
            [self performSegueWithIdentifier:@"presetSettings" sender:self];
//            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"presetSettings"];
//            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
            break;
        case 3:
        {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""message:@"Please enter your password to view the list"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
//            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
//             {
//                 textField.placeholder = NSLocalizedString(@"LoginPlaceholder", @"Login");
//                 [textField addTarget:self
//                               action:@selector(alertTextFieldDidChange:)
//                     forControlEvents:UIControlEventEditingChanged];
//             }];
            
            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
             {
                 textField.placeholder = NSLocalizedString(@"PasswordPlaceholder", @"Password");
                 textField.secureTextEntry = YES;
             }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action)
                                       {
                                           UITextField *password = alertController.textFields.firstObject;
                                           NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                           NSString *password2 = [prefs stringForKey:@"password"];
                                           if (password2 == password.text)
                                           {
                                               [self performSegueWithIdentifier:@"saftyLocation" sender:self];
                                           }
                                           else
                                           {
                                               [self.view makeToast:@"Please enter correct password." duration:3 position:CSToastPositionCenter];
                                               UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""message:@"Please enter your password to view the list"
                                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                               
                                               //            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
                                               //             {
                                               //                 textField.placeholder = NSLocalizedString(@"LoginPlaceholder", @"Login");
                                               //                 [textField addTarget:self
                                               //                               action:@selector(alertTextFieldDidChange:)
                                               //                     forControlEvents:UIControlEventEditingChanged];
                                               //             }];
                                               
                                               [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
                                                {
                                                    textField.placeholder = NSLocalizedString(@"PasswordPlaceholder", @"Password");
                                                    textField.secureTextEntry = YES;
                                                }];
                                               
                                               UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                                                                                      style:UIAlertActionStyleDefault
                                                                                                    handler:^(UIAlertAction *action)
                                                                              {
                                                                                  NSLog(@"Cancel action");
                                                                              }];
                                               
                                               UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                                                                  style:UIAlertActionStyleDefault
                                                                                                handler:^(UIAlertAction *action)
                                                                          {
                                                                              UITextField *password = alertController.textFields.firstObject;
                                                                              NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                                              NSString *password2 = [prefs stringForKey:@"password"];
                                                                              if (password2 == password.text)
                                                                              {
                                                                                  [self performSegueWithIdentifier:@"EmergencyContacts" sender:self];
                                                                              }
                                                                              else
                                                                              {
                                                                                  [self.view makeToast:@"Please enter correct password." duration:3 position:CSToastPositionCenter];
                                                                                  
                                                                                  
                                                                              }
                                                                              
                                                                              
                                                                              NSLog(@"OK action");
                                                                          }];
                                               
                                               
                                               [alertController addAction:cancelAction];
                                               [alertController addAction:okAction];
                                               
                                               [self presentViewController:alertController animated:YES completion:nil];
                                        
                                           }

                                           
                                           NSLog(@"OK action");
                                       }];
            
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];

            
          break;
        }
//            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
            
        case 4:
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""message:@"Please enter your password to view the list"
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            //            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
            //             {
            //                 textField.placeholder = NSLocalizedString(@"LoginPlaceholder", @"Login");
            //                 [textField addTarget:self
            //                               action:@selector(alertTextFieldDidChange:)
            //                     forControlEvents:UIControlEventEditingChanged];
            //             }];
            
            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
             {
                 textField.placeholder = NSLocalizedString(@"PasswordPlaceholder", @"Password");
                 textField.secureTextEntry = YES;
             }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"Cancel action");
                                           }];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action)
                                       {
                                           UITextField *password = alertController.textFields.firstObject;
                                           NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                           NSString *password2 = [prefs stringForKey:@"password"];
                                           if (password2 == password.text)
                                           {
                                               [self performSegueWithIdentifier:@"EmergencyContacts" sender:self];
                                           }
                                           else
                                           {
                                               [self.view makeToast:@"Please enter correct password." duration:3 position:CSToastPositionCenter];
                                               
                                               UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@""message:@"Please enter your password to view the list"
                                                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                                               
                                               //            [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
                                               //             {
                                               //                 textField.placeholder = NSLocalizedString(@"LoginPlaceholder", @"Login");
                                               //                 [textField addTarget:self
                                               //                               action:@selector(alertTextFieldDidChange:)
                                               //                     forControlEvents:UIControlEventEditingChanged];
                                               //             }];
                                               
                                               [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
                                                {
                                                    textField.placeholder = NSLocalizedString(@"PasswordPlaceholder", @"Password");
                                                    textField.secureTextEntry = YES;
                                                }];
                                               
                                               UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                                                                                      style:UIAlertActionStyleDefault
                                                                                                    handler:^(UIAlertAction *action)
                                                                              {
                                                                                  NSLog(@"Cancel action");
                                                                              }];
                                               
                                               UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                                                                  style:UIAlertActionStyleDefault
                                                                                                handler:^(UIAlertAction *action)
                                                                          {
                                                                              UITextField *password = alertController.textFields.firstObject;
                                                                              NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                                              NSString *password2 = [prefs stringForKey:@"password"];
                                                                              if (password2 == password.text)
                                                                              {
                                                                                  [self performSegueWithIdentifier:@"EmergencyContacts" sender:self];
                                                                              }
                                                                              else
                                                                              {
                                                                                  [self.view makeToast:@"Please enter correct password." duration:3 position:CSToastPositionCenter];
                                                                                  
                                                                                  
                                                                              }
                                                                              
                                                                              
                                                                              NSLog(@"OK action");
                                                                          }];
                                               
                                               
                                               [alertController addAction:cancelAction];
                                               [alertController addAction:okAction];
                                               
                                               [self presentViewController:alertController animated:YES completion:nil];
                                           }
                                           
                                           
                                           NSLog(@"OK action");
                                       }];
            
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            break;

            
//            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
        }
            break;
        case 5:
            [self performSegueWithIdentifier:@"selfie" sender:self];
//            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
            
            break;
        case 6:{
            UIApplication *application = [UIApplication sharedApplication];
            [application openURL:[NSURL URLWithString: @"http://visionarytechsolutions.us/"] options:@{} completionHandler:nil];
//            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
        }
            break;
        case 7:{
            
            [self performSegueWithIdentifier:@"instruction" sender:self];
//            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
        }
            break;
        case 8:{
            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
        }
            break;
        case 9:{
            [self performSegueWithIdentifier:@"about" sender:self];
//            [self.view makeToast:@"Coming Soon" duration:3 position:CSToastPositionCenter];
        }   break;

        default:
            break;
    }
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
//    // Set the title of navigation bar by using the menu items
//    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
//    destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
//    
//    // Set the photo if it navigates to the PhotoView
//    if ([segue.identifier isEqualToString:@"showPhoto"]) {
//        
////        UINavigationController *navController = segue.destinationViewController;
////        PhotoViewController *photoController = [navController childViewControllers].firstObject;
////        NSString *photoFilename = [NSString stringWithFormat:@"%@_photo", [menuItems objectAtIndex:indexPath.row]];
////        photoController.photoFilename = photoFilename;
//    }
}


@end
