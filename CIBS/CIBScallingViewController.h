//
//  CIBScallingViewController.h
//  CIBS
//
//  Created by brst on 1/23/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CIBScallingViewController : UIViewController
@property(nonatomic,assign)int vibrateCount;
@property (nonatomic, retain)NSTimer * vibrateTimer;

@end
