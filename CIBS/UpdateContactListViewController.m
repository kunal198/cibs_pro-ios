//
//  ContactListVC.m
//  CIBS_PRO
//
//  Created by mrinal khullar on 12/14/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import "UpdateContactListViewController.h"
#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import "ContactListCell.h"
#import "UpdateAddContactViewController.h"

@interface UpdateContactListViewController ()
{
    NSMutableArray *arr_Contacts;
}
@end

@implementation UpdateContactListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.contact_tableview.dataSource = self;
     self.contact_tableview.delegate = self;
    // Do any additional setup after loading the view.
    
    arr_Contacts = [[NSMutableArray alloc]init];
    

    NSArray *arr = [[NSArray alloc]initWithArray:[appDelegate.arrayofcontact valueForKey:@"name"] copyItems:YES];
        boolArray=[[NSMutableArray alloc] initWithArray:arr copyItems:YES];
   
    
    
    
    selectedContactArray = [[NSMutableArray alloc] init];
    selectContactBool = false;
    
    ABAddressBookRef addressBook = ABAddressBookCreate();
    
    __block BOOL accessGranted = NO;
    
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // We are on iOS 6
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(semaphore);
        });
        
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        
//        dispatch_release(semaphore);
        
    }
    
    else
    { // We are on iOS 5 or Older
        accessGranted = YES;
        //[self getContactsWithAddressBook:addressBook];
        [self getContacts:addressBook];
        [self.contact_tableview reloadData];
    }
    
    if (accessGranted)
    {
        //[self getContactsWithAddressBook:addressBook];
         [self getContacts:addressBook];
        [self.contact_tableview reloadData];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.contact_tableview reloadData];
}



- (void)getContacts:(ABAddressBookRef)addressBook
{
    NSArray *allData = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
    NSInteger contactCount = [allData count];
    
    for (int i = 0; i < contactCount; i++) {
        ABRecordRef person = CFArrayGetValueAtIndex((__bridge CFArrayRef)allData, i);
        
        NSString *firstName = CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName  = CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        if (firstName) {
            dictionary[@"name"] = firstName;
        }
        if (lastName) {
            dictionary[@"lastName"]  = lastName;
        }
        
        ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
        CFIndex phoneNumberCount = ABMultiValueGetCount(phones);
        
        if (phoneNumberCount > 0) {
            NSString *phone = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, 0));
            dictionary[@"Phone"] = phone;
        }
        
        // or if you wanted to iterate through all of them, you could do something like:
        //
        // for (int j = 0; j < phoneNumberCount; j++) {
        //      NSString *phone = CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, j));
        //
        //     // do something with `phone`
        // }
        
        if (phones) {
            CFRelease(phones);
        }
        
        [arr_Contacts addObject:dictionary];
        NSLog(@"%@",arr_Contacts);
        
        
    }
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [arr_Contacts count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"cell1";
    
    ContactListCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[ContactListCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:MyIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    
    _contact_tableview.separatorColor = [UIColor clearColor];
    
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    cell.selectbutton.tag = indexPath.row;
    
    
    [cell.selectbutton addTarget:self action:@selector(SelectButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.label1.text = [arr_Contacts[indexPath.row] valueForKey:@"name"];
    
  
    if ([boolArray containsObject:[arr_Contacts[indexPath.row] valueForKey:@"name"]])
    {
        [cell.selectbutton setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];

    }
    else
    {
        [cell.selectbutton setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];
    }
    
    if ([arr_Contacts[indexPath.row] valueForKey:@"Phone"] != nil)
    {
        cell.label2.text = [arr_Contacts[indexPath.row] valueForKey:@"Phone"];
    }
    else
    {
        
        if ([arr_Contacts[indexPath.row] valueForKey:@"email"] != nil)
        {
            cell.label2.text = [arr_Contacts[indexPath.row] valueForKey:@"email"];
        }
        else
        {
            cell.label2.text = @"No contact info added";
        }
    }
    
    return cell;
    
}

- (IBAction)DoneButton:(UIButton *)sender
{
    
//    AddContactVC *obj = [[AddContactVC alloc] init];
//    obj.arrayoflist = selectedContactArray;
   
    
    NSLog(@"appDelegate.arrayofcontact=%@",appDelegate.arrayofcontact);
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (IBAction)SelectButton:(UIButton *)sender
{
    selectContactBool=YES;
    
    NSLog(@"%ld",(long)sender.tag);
    NSLog(@"%lu",(unsigned long)boolArray.count);
    NSLog(@"%lu",(unsigned long)contactList.count);
    
    
    NSString *str = [arr_Contacts[sender.tag] valueForKey:@"name"];
    
    if ([boolArray containsObject:str])
    {
        [boolArray removeObject:str];
        [appDelegate.arrayofcontact removeObject:[arr_Contacts objectAtIndex:sender.tag]];
        [sender setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];
    }
    else
    {
       [boolArray addObject:str];
    
        [appDelegate.arrayofcontact addObject:[arr_Contacts objectAtIndex:sender.tag]];
         [sender setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
    }
    
    
    

    
//    if ([boolArray[sender.tag] isEqual:@(NO)])
//    {
//        
//        [boolArray replaceObjectAtIndex:sender.tag withObject:@(YES)];
//
//        [appDelegate.arrayofcontact addObject:[contactList objectAtIndex:sender.tag]];
//        
//        [sender setBackgroundImage:[UIImage imageNamed:@"checked Image"] forState:UIControlStateNormal];
//        
//        // selectContactBool = true;
//        
//    }
//    else
//    {
//        
//        [boolArray replaceObjectAtIndex:sender.tag withObject:@(NO)];
//        
//        [appDelegate.arrayofcontact removeObject:[contactList objectAtIndex:sender.tag]];
//
//        [sender setBackgroundImage:[UIImage imageNamed:@"check Box Image"] forState:UIControlStateNormal];
//        
//        // selectContactBool = false;
//        
//    }
//    
   // NSLog(@"after select/unselect array values are = %@",selectedContactArray);
    

}

@end
