//
//  AddContactVC.h
//  CIBS_PRO
//
//  Created by mrinal khullar on 12/14/16.
//  Copyright © 2016 Brihaspati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AddContactVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    BOOL boolforarray;
    NSString *fullName;
    NSString *emailAddress;
    NSString *phoneNumber;
    NSMutableArray *arrayoflist;
    
    
    BOOL fromTable;

}


@property (retain, nonatomic) IBOutlet UIView *contactSubViewDetails;

@property (retain, nonatomic) IBOutlet UILabel *contactInfoView;
@property (retain, nonatomic) IBOutlet UILabel *contactUncerlineLbl;
@property (retain, nonatomic) IBOutlet UITextField *fullName_TxtField;
@property (retain, nonatomic) IBOutlet UILabel *fullName_underLineLbl;
@property (retain, nonatomic) IBOutlet UITextField *emailAddress_TxtField;
@property (retain, nonatomic) IBOutlet UILabel *email_underLine;
@property (retain, nonatomic) IBOutlet UITextField *phoneNumber_TxtField;
@property (retain, nonatomic) IBOutlet UILabel *phoneNumber_underline;
@property (retain, nonatomic) IBOutlet UITableView *table_view;



- (IBAction)AddManuallyAction:(id)sender;




@end
