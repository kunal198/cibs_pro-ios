//
//  CIBScallingViewController.m
//  CIBS
//
//  Created by brst on 1/23/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

#import "CIBScallingViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
@interface CIBScallingViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *CibsImage;

@end

@implementation CIBScallingViewController
@synthesize vibrateCount,vibrateTimer;

- (void)viewDidLoad {
    [super viewDidLoad];
    _CibsImage.image = [UIImage imageNamed:@"fake_screen.jpg"];
    // Do any additional setup after loading the view.
    vibrateCount = 0;
    
    //put this where ever you want to start your vibrating
    vibrateTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(vibratePhone) userInfo:nil repeats:YES];

//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}
-(void)vibratePhone {
    vibrateCount = vibrateCount +1;
    
    if(vibrateCount <= 100) {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle bundleWithIdentifier:@"com.apple.UIKit"] pathForResource:@"Tock" ofType:@"aiff"]] error:NULL];
        [audioPlayer play];
    }
    else {
        
        //vibrated 5 times already kill timer and stop vibrating
        [vibrateTimer invalidate];
        
    }
}
- (IBAction)callingBtn:(id)sender
{
   [vibrateTimer invalidate];
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
